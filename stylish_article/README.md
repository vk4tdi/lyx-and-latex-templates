# Stylish Article

This layout is a wrapper for the "Stylish Article" class produced by Vel, and available at https://www.latextemplates.com/template/stylish-article.

Installation: 
1. Copy SelfArx.cls into your TeX path (I used ~/texmf/tex/latex) 
2. Run texhash from a command prompt
3. Run LyX and run the Tools > Reconfigure menu option.

The SelfArx.layout file is used as a local layout. The demo file article_3.lyx clones the demo Vel provided in article_3.tex (and uses the demo figures and bibliography), and can be used as a starting point for a new article. The LyX document has a few settings tweaked in Documents > Settings to make things work, which you'll need to do if starting a new doc from scratch:
* Document Class: Local Layout is SelfArx.layout -- needed to make the class available
* Modules: Enable Customisable Lists (enumitem) -- needed to reduce default spacing on lists
* Bibliography: Default style is set to unsrt -- to match the .tex demo file
* Language: Set encoding to Other, Language Default (no inputenc) -- needed to avoid setting clash with the class.


Some Evil Red Text (ERT) is needed to generate some front page content. It goes after the defined fields (Affiliation is the last one in my demo) and before the first Section heading.
```
\maketitle % Output the title and abstract box
\tableofcontents % Output the contents section
\thispagestyle{empty}
```

The colours of the text highlight and shading are set in the layout file. Edit these to suit.
