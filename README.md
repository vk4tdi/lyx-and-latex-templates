# LyX and LaTeX Templates

This project holds a few LyX "layout" files and demo files that I've worked on that might be of use to others.

Each template is in its own directory.
