# QUT Thesis

This template is a Style (.sty) that is derived from utthesis.sty (UT Austin) and customised to meet the requirements of the Queensland University of Technology (as they were in 2013). 

A LyX layout has been created and a set of LyX documents using a master document is provided as a starting point.

I've adopted Linux Libertine (and Linux Biolinum) as the text typeface (using `\usepackage{libertine}` in .sty file) because it looks nice and supports ligatures and international character sets. It's pretty easy to change to something else if you like.

A Bibtex Style and examples of using shortcuts for journal names is included. Shortcuts are used so the same Bibtex library can be used for the thesis (full publication names) and for journal/conference publications that use abbreviated names (IEEE requires this).

## Example theses using this template
* [Assessment of precision timing and real-time data networks for digital substation automation](https://eprints.qut.edu.au/60892/), David M E Ingram, Queensland University of Technology, 2013.
* [Management of multiple heterogeneous unmanned aerial vehicles through capability transparency](https://eprints.qut.edu.au/101572/), Ting (Brendan)Chen, Queensland University of Technology, 2016.

If you know of any others please let me know and I'll add them to the list.